import AsyncStorage from '@react-native-async-storage/async-storage';

export const setClass = async (userClass: any) => {
  try {
    await AsyncStorage.setItem('userClass', JSON.stringify(userClass));
  } catch (e) {}
};
export const getClass = async () => {
  try {
    const userClass = await AsyncStorage.getItem('userClass');
    return JSON.parse(<string>userClass);
  } catch (e) {}
};
export const setTypeExam = async (type: Array<number>) => {
  try {
    await AsyncStorage.setItem('typeexam', JSON.stringify(type));
  } catch (e) {}
};
export const getTypeExam = async () => {
  try {
    const type = await AsyncStorage.getItem('typeexam');
    return JSON.parse(<string>type);
  } catch (e) {}
};
export const setNumberIntoApp = async (number: number) => {
  try {
    await AsyncStorage.setItem('number', JSON.stringify(number));
  } catch (e) {}
};
export const getNumberIntoApp = async () => {
  try {
    const number = await AsyncStorage.getItem('number');
    return JSON.parse(<string>number);
  } catch (e) {}
};
